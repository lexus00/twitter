package pl.edu.agh.ztis.twitter.controller.twitter;

import twitter4j.TwitterException;

public class TwitterExceededResourceException extends Exception {
	private static final long serialVersionUID = 1L;
	private Integer secondsToReset;

	
	public TwitterExceededResourceException(TwitterException ex){
		super(ex);
	}
	
	public TwitterExceededResourceException(Integer secondsToReset) {
		super();
		this.secondsToReset = secondsToReset;
	}


	public TwitterException getTwitterException(){
		return (TwitterException) getCause();
	}
	
	public int getSecondsUntilReset(){
		if ( !cointainsTwitterException() )
			return secondsToReset;
		return getTwitterException().getRateLimitStatus().getSecondsUntilReset();
	}
	
	public boolean cointainsTwitterException(){
		return secondsToReset == null;
	}
}
