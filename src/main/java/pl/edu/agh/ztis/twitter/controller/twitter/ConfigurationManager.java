package pl.edu.agh.ztis.twitter.controller.twitter;

import pl.edu.agh.ztis.twitter.controller.PropertiesService;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class ConfigurationManager {
	
	private static ConfigurationManager instance;
	
	private Configuration twitterConf;
	
	private ConfigurationManager(){
	}
	
	public static synchronized ConfigurationManager getInstance() {
		if (instance == null)
			instance = new ConfigurationManager();
		return instance;
	}

	public Configuration getTwitterConfiguration(){
		if (twitterConf == null){
			PropertiesService ps = PropertiesService.getInstance();
			twitterConf = new ConfigurationBuilder()
				.setOAuthConsumerKey(ps.getProperty("oauth.consumerKey"))
				.setOAuthConsumerSecret(ps.getProperty("oauth.consumerSecret"))
				.setOAuthAccessToken(ps.getProperty("oauth.accessToken"))
				.setOAuthAccessTokenSecret(ps.getProperty("oauth.accessTokenSecret"))
				.build();
		}
		return twitterConf;
	}
}
