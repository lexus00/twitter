package pl.edu.agh.ztis.twitter.controller.twitter;

import org.apache.log4j.Logger;
import pl.edu.agh.ztis.twitter.model.dao.HibernateUtil;
import pl.edu.agh.ztis.twitter.model.pojo.TweetInfo;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

/**
 * Created by Mateusz Drożdż on 02.04.16.
 */
public class TwitterStatusListener implements StatusListener {
    private static final Logger logger = Logger.getLogger(TwitterStatusListener.class);
    private final TwitterDAO dao = new TwitterDAO();

    @Override
    public void onStatus(Status status) {
        dao.save(status);
    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        logger.info("Tweet with id: " + statusDeletionNotice.getStatusId() + " deleted");
        TweetInfo deletedTweet = HibernateUtil.getById(TweetInfo.class, statusDeletionNotice.getStatusId());
        HibernateUtil.delete(deletedTweet);

    }

    @Override
    public void onTrackLimitationNotice(int i) {
        logger.warn("Missed " + i + " tweets\n");
    }

    @Override
    public void onScrubGeo(long l, long l1) {
        logger.debug("StatusListener.onScrubGeo(" + l + "," + l1 + ")");
    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {
        logger.debug("StatusListener.onStallWarning(" + stallWarning + ")");
    }

    @Override
    public void onException(Exception e) {
        logger.error("StatusListener.onException", e);
    }
}
