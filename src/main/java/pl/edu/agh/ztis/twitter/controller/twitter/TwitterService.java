package pl.edu.agh.ztis.twitter.controller.twitter;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import twitter4j.*;
import twitter4j.conf.Configuration;
import static pl.edu.agh.ztis.twitter.Constants.*;

public class TwitterService {
	private static final Logger logger = Logger.getLogger(TwitterService.class);
	
	public static TwitterService instance;
	
	private Twitter twitter;
	private TwitterStream twitterStream;
	
	private TwitterResourceManager twitterResourceManager;
	
	private boolean twitterStreamNotStarted; 
	
	
	/**
	 * ONE per Unique Configuration data\
	 * @param conf unique connection data 
	 */
	private TwitterService(Configuration conf){
		twitter = new TwitterFactory(conf).getInstance();
    	twitterStream = new TwitterStreamFactory(conf).getInstance();
    	twitterResourceManager = new TwitterResourceManager(twitter);
	}
	
	
	public void temp() throws TwitterExceededResourceException{
		twitterResourceManager.updateLimits();
	}
	/*
	 * 			REST API
	 */
	
	
	/**
	 * See also: {@link QueryResult#nextQuery()}
	 */
	public synchronized QueryResult getTweetsByKeywords(String keywords) 
			throws TwitterException, InterruptedException {		
		Query query = new Query(keywords);
        query.setCount(100);    
		return getTweetsByKeywords(query);
	}
	
	/**
	 * See also: {@link QueryResult#nextQuery()}
	 */
	public QueryResult getTweetsByKeywords(Query query) 
			throws TwitterException, InterruptedException {
		try {
			twitterResourceManager.beforeCall(GET_TWEETS_BY_KEYWORDS);
			return twitter.search(query);
		} catch (TwitterException | TwitterExceededResourceException ex){
			if (isExceedResources(ex)){
				long mills = getSecondsUntilReset(ex) * 1000;
				logger.debug("Thread sleep " + mills + "[ms]");
				Thread.sleep(mills);
				return getTweetsByKeywords(query);
			}
			throw (TwitterException)ex;
		} 
	}
	
	
	public ResponseList<User> lookupUsers(String ... screenNames) 
			throws TwitterException, InterruptedException{
		try {
			twitterResourceManager.beforeCall(LOOKUP_USERS);
			return twitter.lookupUsers(screenNames);
		} catch (TwitterException | TwitterExceededResourceException ex){
			if (isExceedResources(ex)){
				long mills = getSecondsUntilReset(ex) * 1000;
				logger.debug("Thread sleep " + mills + "[ms]");
				Thread.sleep(mills);
				return twitter.lookupUsers(screenNames);
			}
			throw (TwitterException)ex;
		}
	}


	public List<Status> getUserTweets(String username, int pageno, int count) 
			throws TwitterException, InterruptedException{	
		return getUserTweets(username, null, new Paging(pageno, count));		
	}
	public List<Status> getUserTweets(long userId, int pageno, int count) 
			throws TwitterException, InterruptedException{	
		return getUserTweets(null, userId, new Paging(pageno, count));		
	}
	private List<Status> getUserTweets(String username, Long userId, Paging paging) 
			throws InterruptedException, TwitterException {
		logger.debug("userId=" + userId + ", pageno=" + paging.getPage());
		try {
			twitterResourceManager.beforeCall(GET_USER_TWEETS);
			return username == null ? twitter.getUserTimeline(userId, paging) : 
					twitter.getUserTimeline(username, paging);
		} catch (TwitterException | TwitterExceededResourceException ex){
			if ( isExceedResources(ex) ){
				long mills = getSecondsUntilReset(ex) * 1000;
				logger.debug("Thread sleep " + mills + "[ms]");
				Thread.sleep(mills);
				return getUserTweets(username, userId, paging);
			}
			throw (TwitterException)ex;
		}
	}

	
	


	public User getUserProfile(String username) 
			throws TwitterException, InterruptedException{
		return getUserProfile(username, null);
	}
	public User getUserProfile(long userId) 
			throws TwitterException, InterruptedException{
		return getUserProfile(null, userId);
	}
	private User getUserProfile(String username, Long userId) 
			throws TwitterException, InterruptedException{
		logger.debug("username=" + username + ", userId=" + userId);
		try {
			twitterResourceManager.beforeCall(GET_USER_PROFILE);
			return username == null ? twitter.showUser(userId) : twitter.showUser(username);
		} catch (TwitterException | TwitterExceededResourceException ex){
			if ( isExceedResources(ex) ){
				long mills = getSecondsUntilReset(ex) * 1000;
				logger.debug("Thread sleep " + mills + "[ms]");
				Thread.sleep(mills);
				return getUserProfile(username, userId);
			}
			throw (TwitterException)ex;
		}
	}
	
	
	/**
	 * See also: {@link IDs#getNextCursor()}
	 * @param userId
	 * @param cursor - first call setCursor(-1) then setCursor( {@link IDs#getNextCursor()} )
	 * @return
	 */
	public IDs getUserFollowers(long userId, long cursor) 
			throws TwitterException, InterruptedException{
		return getUserFollowers(userId, null, cursor);		
	}
	/**
	 * See also: {@link IDs#getNextCursor()}
	 * @param username
	 * @param cursor - first call setCursor(-1) then setCursor( {@link IDs#getNextCursor()} )
	 * @return
	 */
	public IDs getUserFollowers(String username, long cursor) 
			throws TwitterException, InterruptedException{
		return getUserFollowers(null, username, cursor);		
	}
	private IDs getUserFollowers(Long userId, String username, long cursor) 
			throws TwitterException, InterruptedException {
		logger.debug("userId=" + userId +", username=" + username + ", cursor=" + cursor);
		try {
			twitterResourceManager.beforeCall(GET_USER_FOLLOWERS);
			return userId == null ? twitter.getFollowersIDs(username, cursor) :
					twitter.getFollowersIDs(userId, cursor);
		} catch (TwitterException | TwitterExceededResourceException ex){
			if ( isExceedResources(ex) ){
				long mills = getSecondsUntilReset(ex) * 1000;
				logger.debug("Thread sleep " + mills + "[ms]");
				Thread.sleep(mills);
				return getUserFollowers(userId, username, cursor);
			}
			throw (TwitterException)ex;
		}		
	}
	
	
	
	/**
	 * See also: {@link IDs#getNextCursor()}
	 * @param userId
	 * @param cursor - first call setCursor(-1) then setCursor( {@link IDs#getNextCursor()} )
	 * @return
	 * @throws TwitterException
	 * @throws InterruptedException 
	 */
	public IDs getUserFriends(long userId, long cursor) 
			throws TwitterException, InterruptedException{
		return getUserFriends(userId, null, cursor);		
	}
	/**
	 * See also: {@link IDs#getNextCursor()}
	 * @param username
	 * @param cursor - first call setCursor(-1) then setCursor( {@link IDs#getNextCursor()} )
	 * @return
	 * @throws TwitterException
	 * @throws InterruptedException 
	 */
	public IDs getUserFriends(String username, long cursor) 
			throws TwitterException, InterruptedException{
		return getUserFriends(null, username, cursor);		
	}
	private IDs getUserFriends(Long userId, String screenName, long cursor) 
			throws TwitterException, InterruptedException {
		logger.debug("userId=" + userId +", screenName=" + screenName + ", cursor=" + cursor);
		try {
			twitterResourceManager.beforeCall(GET_USER_FRIENDS);
			return userId == null ? twitter.getFriendsIDs(screenName, cursor) :
					twitter.getFriendsIDs(userId, cursor);
		} catch (TwitterException | TwitterExceededResourceException ex){
			if ( isExceedResources(ex) ){
				long mills = getSecondsUntilReset(ex) * 1000;
				logger.debug("Thread sleep " + mills + "[ms]");
				Thread.sleep(mills);
				return getUserFriends(userId, screenName, cursor);
			}
			throw (TwitterException)ex;
		}		
	}
	
	
	
	
	/**
	 * @deprecated use {@link #getRateLimitStatus(String)}
	 * @return
	 * @throws TwitterException
	 * @throws InterruptedException 
	 */
	@Deprecated
	public Map<String, RateLimitStatus> getRateLimitStatus() 
			throws TwitterException, InterruptedException {
//		twitterResourceManager.beforeCall(GET_RATE_LIMIT_STATUS);
		try {
			return twitter.getRateLimitStatus();
		} catch (TwitterException ex) {
			if ( isExceedResources(ex) ){
				long mills = getSecondsUntilReset(ex) * 1000;
				logger.debug("Thread sleep " + mills + "[ms]");
				Thread.sleep(mills);
				return getRateLimitStatus();
			}
			throw (TwitterException)ex;
		}
	}
	
	public RateLimitStatus getRateLimitStatus(String resourceName) 
			throws TwitterExceededResourceException {
		return twitterResourceManager.getRateLimitStatus(resourceName);
	}
	
	
	/*
	 * 		STREAM API
	 */

	public void startTwitterStreamFiltering(StatusListener listener, FilterQuery filterQuery) {
		twitterStream.addListener(listener);
		twitterStream.filter(filterQuery);
	}

	/**
	 * @param listener 
	 */
	public void addTwitterStreamListener(StatusListener listener){
		twitterStream.addListener(listener);
		if (twitterStreamNotStarted)
			twitterStream.sample();
	}	

	
	public static synchronized TwitterService getInstance(){
		if (instance == null)
			instance = new TwitterService(ConfigurationManager.getInstance()
						.getTwitterConfiguration());	
		return instance;
	}
	
	
	private boolean isExceedResources(Exception ex){
		if (ex instanceof TwitterException)
			return ((TwitterException)ex).getRateLimitStatus() != null 
				&& ((TwitterException)ex).getRateLimitStatus().getRemaining() <=0;
		if (ex instanceof TwitterExceededResourceException)
			return true;
		throw new IllegalArgumentException(ex.getClass().getSimpleName());
//		return false;
	}
	private long getSecondsUntilReset(Exception ex) {
		if (ex instanceof TwitterException)
			return ((TwitterException)ex).getRateLimitStatus().getSecondsUntilReset();
		if (ex instanceof TwitterExceededResourceException)
			return ((TwitterExceededResourceException) ex).getSecondsUntilReset();
		throw new IllegalArgumentException(ex.getClass().getSimpleName());
	}
}

