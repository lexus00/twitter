package pl.edu.agh.ztis.twitter;

import pl.edu.agh.ztis.twitter.controller.twitter.TwitterLoaderManager;
import pl.edu.agh.ztis.twitter.controller.twitter.TwitterService;
import pl.edu.agh.ztis.twitter.controller.twitter.TwitterStatusListener;
import pl.edu.agh.ztis.twitter.util.KeyWords;
import twitter4j.FilterQuery;
import twitter4j.StatusListener;

public class App {
	
	
	public static void main( String[] args ) throws Exception {

		/** START REST API */
		new TwitterLoaderManager(KeyWords.loadKeywordsRest()).start();

		
		/** START STREAM API */
		StatusListener listener = new TwitterStatusListener();
		FilterQuery filter = new FilterQuery();
		filter.track(KeyWords.loadKeywords());
		TwitterService.getInstance().startTwitterStreamFiltering(listener, filter);
	}
}
