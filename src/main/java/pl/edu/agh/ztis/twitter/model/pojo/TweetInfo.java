package pl.edu.agh.ztis.twitter.model.pojo;

import static pl.edu.agh.ztis.twitter.Constants.TYPE_DATE;

import java.util.Date;

import javax.persistence.*;

import twitter4j.Status;


@Entity
@Table(name="TWEETS")
public class TweetInfo {
	
	// SUPPORT / SAVED
	
	// List<User> userMentioned
	// List<Tag> tags;
	
	
	// NOT SUPPORT / NOT SAVED
	//	List<UserMentionEntity> userMentionedEntities;
	//	List<URLEntity> urlEntities;
	//	List<HashtagEntity> hashtagEntities;
	//	List<MediaEntity> mediaEntities;
	//	List<SymbolEntity> symbolEntities;
	//	List<String> withHeldInCountries;
	
	@Id
	private long id;
	
	@Column(columnDefinition=TYPE_DATE)
	private Date created;

	@Column(length=140)
	private String text;
	
	@Column
	private String source;

	@Column(name="is_truncated")
	private boolean truncated;

	@Column(name="in_reply_to_status_id")
	private long inReplyToStatusId;

	@Column(name="in_reply_to_user_id")
	private long inReplyToUserId;

	@Column(name="is_favorited")
	private boolean favorited;

	@Column(name="is_retweeted")
	private boolean retweeted;

	@Column(name="favorite_count")
	private int favoriteCount;

	@Column(name="in_reply_to_screen_name")
	private String inReplyToScreenName;

	@Column(name="retweet_count")
	private int retweetCount;

	@Column(name="is_possibly_sensitive")
	private boolean possiblySensitive;

	@Column
	private String lang;

	@Column(name="retweeted_id")
	private Long retweetedId;
	
	@Column(name="current_user_retweet_id")
	private Long currentUserRetweetId;

	@Column(name="user_id")
	private long userId;
	
	@Column(name="quoted_status_id")
	private Long quotedStatusId;

	@Column(columnDefinition=TYPE_DATE, name="fetched_to")
	private Date fetchedTo;

	@Column(name = "location_id")
	private Long locationId;

	@Column(name = "place_id")
	private String placeId;

	
	public TweetInfo(){
	}	
	public TweetInfo(long id){
		this.id = id;
	}
	
	public TweetInfo(Status tweet, Long locationId, String placeId) {
		this.id = tweet.getId();
		this.created = tweet.getCreatedAt();
		this.text = tweet.getText();
		this.source = tweet.getSource();
		this.truncated = tweet.isTruncated();
		this.inReplyToStatusId = tweet.getInReplyToStatusId();
		this.inReplyToUserId = tweet.getInReplyToUserId();
		this.favorited = tweet.isFavorited();
		this.retweeted = tweet.isTruncated();
		this.favoriteCount = tweet.getFavoriteCount();
		this.inReplyToScreenName = tweet.getInReplyToScreenName();
		this.locationId = locationId;
		this.retweetCount = tweet.getRetweetCount();
		this.possiblySensitive = tweet.isPossiblySensitive();
		this.lang = tweet.getLang();
		if (tweet.isRetweet())
			this.retweetedId = tweet.getRetweetedStatus().getId();
		this.currentUserRetweetId = tweet.getCurrentUserRetweetId();
		if (tweet.getUser() != null)
			this.userId = tweet.getUser().getId();
		this.quotedStatusId = tweet.getQuotedStatusId();
		this.placeId = placeId;
	}
	
	
	
	// getters and setters
	public Date getCreated() {
		return created;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public boolean isTruncated() {
		return truncated;
	}
	public void setTruncated(boolean isTruncated) {
		this.truncated = isTruncated;
	}
	public long getInReplyToStatusId() {
		return inReplyToStatusId;
	}
	public void setInReplyToStatusId(long inReplyToStatusId) {
		this.inReplyToStatusId = inReplyToStatusId;
	}
	public long getInReplyToUserId() {
		return inReplyToUserId;
	}
	public void setInReplyToUserId(long inReplyToUserId) {
		this.inReplyToUserId = inReplyToUserId;
	}
	public boolean isFavorited() {
		return favorited;
	}
	public void setFavorited(boolean favorited) {
		this.favorited = favorited;
	}
	public boolean isRetweeted() {
		return retweeted;
	}
	public void setRetweeted(boolean retweeted) {
		this.retweeted = retweeted;
	}
	public String getInReplyToScreenName() {
		return inReplyToScreenName;
	}
	public void setInReplyToScreenName(String inReplyToScreenName) {
		this.inReplyToScreenName = inReplyToScreenName;
	}
	public int getRetweetCount() {
		return retweetCount;
	}
	public void setRetweetCount(int retweetCount) {
		this.retweetCount = retweetCount;
	}
	public boolean isPossiblySensitive() {
		return possiblySensitive;
	}
	public void setPossiblySensitive(boolean possiblySensitive) {
		this.possiblySensitive = possiblySensitive;
	}
	public String getLang() {
		return lang;
	}
	public void setLang(String lang) {
		this.lang = lang;
	}
	public Long getRetweetedId() {
		return retweetedId;
	}
	public void setRetweetedId(Long retweetedId) {
		this.retweetedId = retweetedId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Long getQuotedStatusId() {
		return quotedStatusId;
	}
	public void setQuotedStatusId(Long quotedStatusId) {
		this.quotedStatusId = quotedStatusId;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public int getFavoriteCount() {
		return favoriteCount;
	}
	public void setFavoriteCount(int favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
	public Long getCurrentUserRetweetId() {
		return currentUserRetweetId;
	}
	public void setCurrentUserRetweetId(Long currentUserRetweetId) {
		this.currentUserRetweetId = currentUserRetweetId;
	}
	public Date getFetchedTo() {
		return fetchedTo;
	}
	public void setFetchedTo(Date fetchedTo) {
		this.fetchedTo = fetchedTo;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public String getPlaceId() {
		return placeId;
	}
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}		
}
