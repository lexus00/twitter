package pl.edu.agh.ztis.twitter.controller.twitter;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import pl.edu.agh.ztis.twitter.model.dao.HibernateUtil;
import pl.edu.agh.ztis.twitter.model.pojo.Location;
import pl.edu.agh.ztis.twitter.model.pojo.Place;
import pl.edu.agh.ztis.twitter.model.pojo.Tag;
import pl.edu.agh.ztis.twitter.model.pojo.TweetInfo;
import pl.edu.agh.ztis.twitter.model.pojo.UserInfo;
import twitter4j.GeoLocation;
import twitter4j.HashtagEntity;
import twitter4j.IDs;
import twitter4j.Status;
import twitter4j.User;
import twitter4j.UserMentionEntity;

public class TwitterDAO {
    @SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(TwitterDAO.class);
	
    private static final String TWEET__TAG__INSERT = "INSERT INTO tweet__tag(tweet_id, tag_id) VALUES (?, ?);";
    private static final String TWEET__TAG__SELECT = "select count(*) from tweet__tag where tweet_id =? and tag_id=?";
    private static final String TWEET__USER_MENTIONED__INSERT = "INSERT INTO tweet__user_mentioned(tweet_id, user_id) VALUES (?, ?);";
    private static final String TWEET__USER_MENTIONED__SELECT = "select count(*) from tweet__user_mentioned where tweet_id=? and user_id=?";
    
    private static final String USER_FOLLOWERS__INSERT = "INSERT INTO user_followers(user_id, follower_id) VALUES (?, ?);";
    private static final String USER_FOLLOWERS__SELECT = "SELECT count(*) from user_followers where user_id=? and follower_id=?";
    private static final String USER_FRIENDS__INSERT = "INSERT INTO user_friends(user_id, friend_id) VALUES (?, ?);";
    private static final String USER_FRIENDS__SELECT = "select count(*) from user_friends where user_id=? and friend_id=?;";
    
    
    private static final String TAG_NOT_FETCH__MOST_COMMON = "select tag.value from tweet__tag  tt "
    		+ "inner join tags tag on tt.tag_id = tag.id where not tag.is_fetched or tag.is_fetched is null "
    		+ "group by tag.value order by count(*) DESC limit ?";
    
    private static final String USER_NOT_FETCH__FRIENDS = "select fk.friend_id as \"user_id\", count(*) from user_friends fk "
    		+ "where (select is_fetched from users where id=fk.friend_id) is not true group by fk.friend_id order by count(*) DESC limit ?";
    private static final String USER_NOT_FETCH__MENTIONED = "select fk.user_id, count(*) from tweet__user_mentioned fk "
    		+ "where (select is_fetched from users where id=fk.user_id) is not true group by fk.user_id order by count(*) DESC limit ?";
    private static final String USER_NOT_FETCH__FOLLOWERS = "select fk.follower_id as \"user_id\", count(*) from user_followers fk "
    		+ "where (select is_fetched from users where id=fk.follower_id) is not true group by fk.follower_id order by count(*) DESC limit ?";
    
    
    
	public Long save(User user){
		if (user == null)
			return null;	
		if ( ! HibernateUtil.exists(UserInfo.class, user.getId()))
			HibernateUtil.save(new UserInfo(user));	
		return user.getId();
	}
	
	public void saveFollowerRelation(long userId, IDs followers){
		Session session = HibernateUtil.openSession();
        session.beginTransaction();
              
        for (long followerId : followers.getIDs())
    		checkAndInsert(session, 
    			USER_FOLLOWERS__INSERT, USER_FOLLOWERS__SELECT, 
    			userId, followerId);
        
        session.getTransaction().commit();
        session.close();
	}
	
	public void saveFriendRelation(long userId, IDs friends){
		Session session = HibernateUtil.openSession();
        session.beginTransaction();
              
        for (long friendId : friends.getIDs())
    		checkAndInsert(session, 
				USER_FRIENDS__INSERT, USER_FRIENDS__SELECT, 
				userId, friendId);
        
        session.getTransaction().commit();
        session.close();
	}
	
	
	
	public void save(Status tweet){
		if (tweet == null)
			return;
		if ( HibernateUtil.exists(TweetInfo.class, tweet.getId()) )
			return;
						
		Long locationId = getId(tweet.getGeoLocation());
		String placeId = getId(tweet.getPlace());

		save(tweet.getUser());
		save(tweet.getQuotedStatus());
		save(tweet.getRetweetedStatus());
		
		TweetInfo tweetInfo = new TweetInfo(tweet, locationId, placeId);
        HibernateUtil.saveOrUpdate(tweetInfo);
        
                        
        Session session = HibernateUtil.openSession();
        session.beginTransaction();
        
        HashtagEntity[] tags = tweet.getHashtagEntities();
        if (tags != null && tags.length > 0)
	        for ( HashtagEntity tag : tags)
        		checkAndInsert(session, 
        			TWEET__TAG__INSERT, TWEET__TAG__SELECT, 
        			tweetInfo.getId(), getId(tag));
        
        UserMentionEntity[] mentioneds = tweet.getUserMentionEntities();
        if (mentioneds != null && mentioneds.length > 0)
        	for (UserMentionEntity mentioned : mentioneds)
        		checkAndInsert(session, 
    				TWEET__USER_MENTIONED__INSERT,TWEET__USER_MENTIONED__SELECT, 
    				tweetInfo.getId(), mentioned.getId());
        
        session.getTransaction().commit();
        session.close();
	}
		
	
	/** save the object if necessary */
	public Long getId(GeoLocation geoLocation){
		if (geoLocation == null)
			return null;
		Location location = new Location(geoLocation);
		Session session = HibernateUtil.openSession();		
		Location locationDB = (Location) session.createCriteria(Location.class)
			.add(Restrictions.eq("latitude", location.getLatitude()))
			.add(Restrictions.eq("longitude", location.getLongitude()))
			.uniqueResult();
		session.close();
		if ( locationDB != null)
			return locationDB.getId();
		
		HibernateUtil.save(location);
		return location.getId();
	}
	
	/** save the object if necessary */
	public String getId(twitter4j.Place place){
		if (place == null)
			return null;
		if ( ! HibernateUtil.exists(Place.class, place.getId()))
			HibernateUtil.save(new Place(place));
		return place.getId();
	}
	
	public long getId(HashtagEntity htag){		
		Tag tag = getTag(htag.getText());
		if (tag == null){
			tag = new Tag(htag);
			HibernateUtil.save(tag);
		}		
		return tag.getId();
	}
	
	public Tag getTag(String value){
		Session session = HibernateUtil.openSession();
		Tag tag = (Tag) session.createCriteria(Tag.class)
			.add(Restrictions.eq("value", value))
			.uniqueResult();
		session.close();				
		return tag;
	}
	
	private static void checkAndInsert(Session session, String insert, 
			String select, long arg0, long arg1){		
		Long rowcount = ((Number) session.createSQLQuery(select)
				.setLong(0, arg0)
				.setLong(1, arg1)
				.uniqueResult())
			.longValue();
	
		if (rowcount == null || rowcount <= 0)
			session.createSQLQuery(insert)
				.setLong(0, arg0)
				.setLong(1, arg1)
				.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getNotFetchTagsByMostCommon(int nrows){
		Session session = HibernateUtil.openSession();
		List<String> list = session.createSQLQuery(TAG_NOT_FETCH__MOST_COMMON)
			.addScalar("value", new StringType())
			.setParameter(0, nrows)
			.list();
		session.close();
		return list;
	}

	public List<Long> getNotFetchUserFriends(int nrows){
		return getNotFetchUsers(USER_NOT_FETCH__FRIENDS, nrows);
	}
	public List<Long> getNotFetchUserFollowers(int nrows){
		return getNotFetchUsers(USER_NOT_FETCH__FOLLOWERS, nrows);
	}
	public List<Long> getNotFetchUserMentioned(int nrows){
		return getNotFetchUsers(USER_NOT_FETCH__MENTIONED, nrows);
	}	
	@SuppressWarnings("unchecked")
	private List<Long> getNotFetchUsers(String query, int nrows){
		Session session = HibernateUtil.openSession();
		List<Long> list = session.createSQLQuery(query)
			.addScalar("user_id", new LongType())
			.setParameter(0, nrows)
			.list();
		session.close();
		return list;
	}
}
