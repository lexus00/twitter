package pl.edu.agh.ztis.twitter.controller.twitter;

import java.util.Arrays;

import org.apache.log4j.Logger;

import twitter4j.GeoLocation;
import twitter4j.Place;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.User;

public class LoggingStatusListener implements StatusListener {
	private static final Logger logger = Logger.getLogger(LoggingStatusListener.class);
	
	public void onStatus(Status status) {
        logger.info(buildStatusInfo(status));
 }

	@Override
	public void onException(Exception arg0) {
		logger.error("StatusListener.onException", arg0);
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		logger.info("Status with id: " + arg0.getStatusId() + " deleted");
	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
     logger.info("StatusListener.onScrubGeo(" + arg0 + "," + arg1 + ")");
	}

	@Override
	public void onStallWarning(StallWarning arg0) {
     logger.info("StatusListener.onStallWarning(" + arg0 + ")");
	}

	@Override
	public void onTrackLimitationNotice(int arg0) {
		logger.warn("Missed " + arg0 + " tweets\n");
	}
	
	public static final String buildStatusInfo(Status status) {
        User user = status.getUser();
        String content = status.getText();
        GeoLocation geolocation = status.getGeoLocation();
        Place place = status.getPlace();

        StringBuilder sb = new StringBuilder();
        sb.append(user.getId()); sb.append(", ");
        sb.append(user.getName()); sb.append(", ");
        sb.append(user.getScreenName()); sb.append(", ");
        if (user != null){
            sb.append(user.getLang()); sb.append(", ");
            sb.append(user.getLocation()); sb.append(", ");
            sb.append(user.getTimeZone()); sb.append(", ");
            sb.append(Arrays.toString(user.getWithheldInCountries())); sb.append(", ");
        }
        if (place != null){
            sb.append(place.getCountry()); sb.append(", ");
            sb.append(place.getCountryCode()); sb.append(", ");
            sb.append(place.getFullName()); sb.append(", ");
        }

        if (geolocation != null){
            sb.append(geolocation.getLatitude()); sb.append(":");
            sb.append(geolocation.getLongitude()); sb.append(", ");
        }
        sb.append(content);

        return sb.toString();
    }
}
