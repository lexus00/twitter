package pl.edu.agh.ztis.twitter.util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author mdrozdz
 */
public class KeyWords {

    @SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(KeyWords.class);
    private static final String KEYWORDS = "keywords";
    private static final String KEYWORDS_REST = "keywords_rest";

    public static String[] loadKeywords() {
        List<String> list = load(KEYWORDS);
        return list.toArray(new String[list.size()]);
    }

    public static String[] loadKeywordsRest() {
        List<String> list = load(KEYWORDS_REST);
        return list.toArray(new String[list.size()]);
    }

    private static List<String> load(String filename) {
        try (Stream<String> stream = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(filename))).lines()) {
            return stream.collect(Collectors.toList());
        }
    }
}
