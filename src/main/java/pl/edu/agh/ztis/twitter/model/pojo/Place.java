package pl.edu.agh.ztis.twitter.model.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PLACES")
public class Place {
	
	@Id
	private String id;
	@Column
	private String country;
	@Column(name="country_code")
	private String countryCode;
	@Column
	private String name;
	@Column
	private String fullName;
	@Column(name = "street_address")
	private String streetAddress;
	@Column(name = "place_type")
	private String placeType;
	@Column(name = "url")
	private String url;

    public Place() {
    }

    public Place(twitter4j.Place place) {
		this.id = place.getId();
		this.country = place.getCountry();
		this.countryCode = place.getCountryCode();
		this.name = place.getName();
		this.fullName = place.getFullName();
		this.streetAddress = place.getStreetAddress();
		this.countryCode = place.getPlaceType();
		this.url = place.getURL();
	}

	// getters and setters
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPlaceType() {
		return placeType;
	}

	public void setPlaceType(String placeType) {
		this.placeType = placeType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
