package pl.edu.agh.ztis.twitter.model.pojo;

import twitter4j.GeoLocation;

import javax.persistence.*;

@Entity
@Table(name="LOCATIONS")
public class Location {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    public Location() {
    }

    public Location(GeoLocation geoLocation) {
        this.latitude = geoLocation.getLatitude();
        this.longitude = geoLocation.getLongitude();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
