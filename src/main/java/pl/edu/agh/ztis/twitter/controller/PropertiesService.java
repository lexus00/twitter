package pl.edu.agh.ztis.twitter.controller;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesService {
	private static final Logger logger = Logger.getLogger(PropertiesService.class);
	private static final boolean IS_PROD_ENV = true;
	
	private static PropertiesService instance;
	private Properties properties;
	
	static {
		instance = new PropertiesService();
	}
	
	private PropertiesService(){
		try {
			String src = IS_PROD_ENV ? "twitter.properties" : "twitter_test.properties";
			
			InputStream is = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream(src); 
			Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
			properties = new Properties();
			properties.load(reader);
			reader.close();
			is.close();			
		} catch (Exception ex){
			logger.error(ex.getMessage(), ex);
			System.exit(-1);
			
		}
	}
	
	public static PropertiesService getInstance(){
		return instance;
	}
	

	public Properties getProperties() {
		return properties;
	}
	public String getProperty(String key){
		return properties.getProperty(key);
	}
}
