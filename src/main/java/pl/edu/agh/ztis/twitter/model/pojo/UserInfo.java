package pl.edu.agh.ztis.twitter.model.pojo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import pl.edu.agh.ztis.twitter.Constants;
import twitter4j.User;

@Entity
@Table(name="USERS")
public class UserInfo {
	
	// SUPPORT / SAVED
	// List<UserInfo> followers;
	// List<UserInfo> friends
	
	@Id
	private long id;
	
	@Column
	private String name;
	
	@Column
	private String screenName;

	@Column
	private String language;
	
	@Column
	private String location;
	
	@Column 
	private String description;
	
	@Column(name="followers_count")
	private Integer followersCount;
	
	@Column(name="friends_count")
	private Integer friendsCount;
	
	@Column(name="created_at", columnDefinition=Constants.TYPE_DATE)
	private Date createdAt;
	
	@Column(name="favourites_count")
	private int favouritesCount;
	
	@Column(name="statuses_count")
	private int statusesCount;
	
	@Column(name="is_verified")
	private boolean verified;
	
	@Column(name="is_protected")
	private boolean protectedd;
	
	@Column(name="is_default_profile")
	private boolean defaultProfile;
	
	
	@Column(name="is_fetched")
	private boolean fetched;
	
	@Column(name="fetched_to_date", columnDefinition=Constants.TYPE_DATE)
	private Date fetchedToDate;
	
	
	
	public UserInfo(){
	}
	public UserInfo(long id){
		this.id = id;
	}
	
	public UserInfo(User user){
		this.id = user.getId();
		this.name = user.getName();
		this.screenName = user.getScreenName();
		this.language = user.getLang();
		this.location = user.getLocation();
		this.description = user.getDescription();
		this.followersCount = user.getFollowersCount();
		this.friendsCount = user.getFriendsCount();
		this.createdAt = user.getCreatedAt();
		this.favouritesCount = user.getFavouritesCount();
		this.statusesCount = user.getStatusesCount();
		this.verified = user.isVerified();
		this.protectedd = user.isProtected();
		this.defaultProfile = user.isDefaultProfile();
		this.fetched = false;
	}
	
	
	
	// getters and setters
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getScreenName() {
		return screenName;
	}
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getFollowersCount() {
		return followersCount;
	}
	public void setFollowersCount(Integer followersCount) {
		this.followersCount = followersCount;
	}
	public Integer getFriendsCount() {
		return friendsCount;
	}
	public void setFriendsCount(Integer friendsCount) {
		this.friendsCount = friendsCount;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public int getFavouritesCount() {
		return favouritesCount;
	}
	public void setFavouritesCount(int favouritesCount) {
		this.favouritesCount = favouritesCount;
	}
	public int getStatusesCount() {
		return statusesCount;
	}
	public void setStatusesCount(int statusesCount) {
		this.statusesCount = statusesCount;
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public boolean isFetched() {
		return fetched;
	}
	public boolean isProtectedd() {
		return protectedd;
	}
	public void setProtectedd(boolean protectedd) {
		this.protectedd = protectedd;
	}
	public boolean isDefaultProfile() {
		return defaultProfile;
	}
	public void setDefaultProfile(boolean defaultProfile) {
		this.defaultProfile = defaultProfile;
	}
	public Date getFetchedToDate() {
		return fetchedToDate;
	}
	public void setFetchedToDate(Date fetchedToDate) {
		this.fetchedToDate = fetchedToDate;
	}
	public void setFetched(boolean fetched) {
		this.fetched = fetched;
	}
}
