package pl.edu.agh.ztis.twitter.controller.twitter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import twitter4j.RateLimitStatus;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import static pl.edu.agh.ztis.twitter.Constants.UPDATE_TWITTER_LIMITS_EVERY;

public class TwitterResourceManager {
	private static final Logger logger = 
			Logger.getLogger(TwitterExceededResourceException.class);
	
	private Twitter service;
	
	private Date lastUpdate;
	private int offset;	// in seconds
	
	private Map<String, RateLimitStatus> resourceLimits;
	private Map<String, Integer> remainings;
	
	
	public TwitterResourceManager(Twitter twitterService) {
		this.service = twitterService;
		this.lastUpdate = new Date(10000);
		this.remainings = new HashMap<>();
	}

	public RateLimitStatus getRateLimitStatus(String resourceName) 
			throws TwitterExceededResourceException {
		updateLimitsIfNeed();
		return resourceLimits.get(resourceName);
	}
	
	public synchronized void beforeCall(String resourceName) 
			throws TwitterExceededResourceException {
		synchronized (remainings) {
			updateLimitsIfNeed();
			
			int remaining = remainings.get(resourceName);
//			logger.debug(resourceLimits + " remaining " + remaining + " calls");
			if (remaining <= 0)
				throw new TwitterExceededResourceException(
					getServerSecondsToReset(resourceLimits.get(resourceName)) + offset);
			else
				remainings.put(resourceName, --remaining);	
		}			
	}
	
	public synchronized void updateLimitsIfNeed() 
			throws TwitterExceededResourceException{
		if (new Date().getTime() - lastUpdate.getTime() > UPDATE_TWITTER_LIMITS_EVERY)
			updateLimits();
	}
	
	public void updateLimits() throws TwitterExceededResourceException {
		synchronized (remainings) {
			try {
				resourceLimits = service.getRateLimitStatus();
				resourceLimits.entrySet().stream().forEach(entry -> 
					remainings.put(entry.getKey(), entry.getValue().getRemaining()));	
				
				RateLimitStatus limit = resourceLimits.values().stream().findAny().get();			
				// +1 to ensure resource-limit reset 
				offset = limit.getSecondsUntilReset() - getServerSecondsToReset(limit) + 1;	
				logger.debug("update limits, offset=" + offset);	
				lastUpdate = new Date();
			} catch (TwitterException ex) {
				throw new TwitterExceededResourceException(ex);
			}
		}	
	}
	
	private int getServerSecondsToReset(RateLimitStatus limit){	
		return limit.getResetTimeInSeconds() - (int)(new Date().getTime()/1000);
	}

	
}
