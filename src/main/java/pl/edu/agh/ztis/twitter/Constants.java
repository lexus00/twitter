package pl.edu.agh.ztis.twitter;

public class Constants {
	
	public static final String TYPE_DATE_MSSQL = "datetime";
	public static final String TYPE_DATE_PSQL = "timestamp without time zone";
	
	public static final String TYPE_DATE = TYPE_DATE_PSQL;
	

	// Twitter resource name 
	public static final String GET_RATE_LIMIT_STATUS = "/application/rate_limit_status";	
	public static final String GET_TWEETS_BY_KEYWORDS = "/search/tweets";
	public static final String GET_USER_PROFILE  = "/users/show/:id";
	public static final String GET_USER_TWEETS = "/statuses/user_timeline";
	public static final String GET_USER_FOLLOWERS = "/followers/ids";
	public static final String GET_USER_FRIENDS = "/friends/ids";
	public static final String LOOKUP_USERS = "/users/lookup";

	public static final long UPDATE_TWITTER_LIMITS_EVERY = 60 *1000; // every minute
}
