package pl.edu.agh.ztis.twitter.model.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.reflections.Reflections;
import pl.edu.agh.ztis.twitter.controller.PropertiesService;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.List;
import java.util.Set;


public class HibernateUtil {
	private static Logger logger = Logger.getLogger(HibernateUtil.class);
	private static SessionFactory sessionFactory;

	private HibernateUtil(){
	}
	

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try { 
				Configuration conf = new Configuration()
					.setProperties(PropertiesService.getInstance().getProperties());
								
				Reflections reflections = 
					new Reflections("pl.edu.agh.ztis.twitter.model.pojo");
				Set<Class<?>> classes = reflections
						.getTypesAnnotatedWith(Entity.class);
				for (final Class<?> clazz : classes)
					conf.addAnnotatedClass(clazz);

				StandardServiceRegistry ssr = new StandardServiceRegistryBuilder()
						.applySettings(conf.getProperties()).build();
								
				sessionFactory = conf.buildSessionFactory(ssr);
				
				Session session = sessionFactory.openSession();
				if (isNotSchemaFullyCreated(session))
					createRemainnigObjectsInSchema(session);
				session.close();
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new ExceptionInInitializerError(e);
			}
		}
		return sessionFactory;
	}
	
	public static Session openSession(){
		return getSessionFactory().openSession();
	}


	public static String saveOrUpdate(Object obj) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(obj);
			session.flush();
			session.clear();
			session.getTransaction().commit();
			return null;
		} catch (Exception ex) {
			session.getTransaction().rollback();
			logger.error(ex.getMessage(), ex);
			return ex.getMessage();
		} finally {
			session.close();
		}
	}

	public static String saveOrUpdateAll(List<?> list) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			for (Object obj : list)
				session.saveOrUpdate(obj);
			session.flush();
			session.clear();
			session.getTransaction().commit();
			return null;
		} catch (Exception ex) {
			session.getTransaction().rollback();
			logger.error(ex.getMessage(), ex);
			return ex.getMessage();
		} finally {
			session.close();
		}
	}
	
	public static String save(Object obj) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.save(obj);
			session.flush();
			session.clear();
			session.getTransaction().commit();
			return null;
		} catch (Exception ex) {
			session.getTransaction().rollback();
			logger.error(ex.getMessage(), ex);
			return ex.getMessage();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T getById(Class<T> clazz, Serializable id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		T obj = (T) session.get(clazz, id);
		session.close();
		return obj;
	}
	
	public static boolean exists(Class<?> clazz, Serializable id) {
		return getById(clazz, id) != null;
	}

	public static String delete(Object obj) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.delete(obj);
			session.flush();
			session.clear();
			session.getTransaction().commit();
			return null;
		} catch (Exception ex) {
			session.getTransaction().rollback();
			logger.error(ex.getMessage(), ex);
			return ex.getMessage();
		} finally {
			session.close();
		}
	}
	
	private static void createRemainnigObjectsInSchema(Session session) {
		String[] queries = {
			createCreateJoinTableQuery("tweet__tag", "tweet_id", "tag_id"),
			createCreateJoinTableQuery("tweet__user_mentioned", "tweet_id", "user_id"),			
			createCreateJoinTableQuery("user_followers", "user_id", "follower_id"),
			createCreateJoinTableQuery("user_friends", "user_id", "friend_id"),
		};
		
		session.beginTransaction();
		for (String query : queries)
			session.createSQLQuery(query).executeUpdate();
		session.getTransaction().commit();
		session.flush();
		
	}
	public static String createCreateJoinTableQuery(String tblName, 
			String fk1Name, String fk2Name){
		return String.format("CREATE TABLE %1$s (%2$s bigint NOT NULL, %3$s bigint NOT NULL, "
				+ "CONSTRAINT %1$s_pkey PRIMARY KEY (%2$s, %3$s));", tblName, fk1Name,fk2Name);
	}
	
	private static boolean isNotSchemaFullyCreated(Session session){
		try {			
			session.beginTransaction();
			session.createSQLQuery("select 1 from tweet__tag LIMIT 1").list();
			logger.info("schema is fully created");
			return false;
		} catch (Exception ex){
			return true;
		} finally {
			session.getTransaction().rollback();
			session.clear();
		}
	}
	
}
