package pl.edu.agh.ztis.twitter.controller.twitter;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

public class TwitterLoaderManager {
	private static final Logger logger = Logger.getLogger(TwitterLoaderManager.class);
	
	private TwitterDAO dao;
	private TwitterLoaderThread instance;


	private static final int NROW_LIMITS = 100;
	private static final int SLEEP_WHEN_QUEUE_IS_EMPTY = 300000; // 5 min
	private static final int REST_UPDATE_KEYWORDS_EVERY = 24 * 3600 * 1000; // every day
	
	private LinkedList<Long> queueToLoadUser;	
	private LinkedList<String> queueToLoadTag;
	private List<String> _keywords;
	private Date lastUpdateKeywords;
	
	
	public TwitterLoaderManager(){
		dao = new TwitterDAO();
		instance = new TwitterLoaderThread();
		queueToLoadTag = new LinkedList<>();
		queueToLoadUser = new LinkedList<>();	
		lastUpdateKeywords = new Date();	
	}
	
	public TwitterLoaderManager(String[] keywords){
		this();		
		this._keywords = Arrays.asList(keywords);
		for (String keyword : keywords){
			String keyword2 = keyword.replaceAll("\\s", "");
			this.queueToLoadTag.add(keyword);
			if ( !keyword.equals(keyword2))
				this.queueToLoadTag.add(keyword2);
		}
	}
	
	
	public void start() {
		if (_keywords != null){
			Set<Long> ids = instance.lookupUsersId(_keywords);
			queueToLoadUser.addAll(ids);
		}
				
		Thread threadTagLoad = new Thread(() -> {
			while(true){			
				String keyword = getNextKeyword();
				if (keyword == null){
					try {
						Thread.sleep(SLEEP_WHEN_QUEUE_IS_EMPTY);
					} catch (Exception e) {}
					continue;
				}
				instance.loadTag(keyword);
			}
		});
		
		Thread threadUserDataLoader = new Thread(() -> {
			while(true){			
				Long userId = getNextUserId();
				if (userId == null){
					try {
						Thread.sleep(SLEEP_WHEN_QUEUE_IS_EMPTY);
					} catch (Exception e) {}
					continue;
				}					
				instance.loadUserAllData(userId);
			}
		});
		
		threadTagLoad.start();
		threadUserDataLoader.start();	
	}
	
	private boolean isKeywordsUpdateNeed(){
		return new Date().getTime() - lastUpdateKeywords.getTime() > REST_UPDATE_KEYWORDS_EVERY;
	}
		
	private String getNextKeyword(){
		if (queueToLoadTag.isEmpty()){
			logger.info("isKeywordsUpdateNeed()=" + isKeywordsUpdateNeed());
			if ( isKeywordsUpdateNeed() ){
				queueToLoadTag.addAll( _keywords );
				lastUpdateKeywords = new Date();
			} else {
				queueToLoadTag.addAll(dao.getNotFetchTagsByMostCommon(NROW_LIMITS));
			}			
		}
		String keyword = queueToLoadTag.isEmpty() ? null : queueToLoadTag.pop();
		logger.debug("next keyword = " + keyword);	
		return keyword;
	}
	
	private Long getNextUserId(){
		Long userId = null;
		if (queueToLoadUser.isEmpty()){
			List<Long> list1 = dao.getNotFetchUserFriends(NROW_LIMITS);
			List<Long> list2 = dao.getNotFetchUserMentioned(NROW_LIMITS);
			List<Long> list3 = dao.getNotFetchUserFollowers(NROW_LIMITS);
			
			int added = 0;			
			boolean are_empty = false;
			while (added < NROW_LIMITS && !are_empty){
				added += moveFirstElement(queueToLoadUser, list1);
				added += moveFirstElement(queueToLoadUser, list2);
				added += moveFirstElement(queueToLoadUser, list3);
				are_empty = list1.isEmpty() || list2.isEmpty() || list3.isEmpty();
			}
			
		}
		if (!queueToLoadUser.isEmpty())
			userId = queueToLoadUser.pop();		
		logger.debug("next userId = " + userId);
		return userId;
	} 
	
	private static int moveFirstElement(List<Long> to, List<Long> from){
		if ( from.isEmpty())
			return 0;
		to.add(from.remove(0));
		return 1;
	}
	
}
