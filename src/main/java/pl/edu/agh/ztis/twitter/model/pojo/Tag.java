package pl.edu.agh.ztis.twitter.model.pojo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import pl.edu.agh.ztis.twitter.Constants;
import twitter4j.HashtagEntity;

@Entity
@Table(name="TAGS")
public class Tag {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
		
	@Column(length=140)
	private String value;
	
	@Column(name="is_fetched")
	private Boolean fetched = false;
	
	@Column(name="fetched_to_date", columnDefinition=Constants.TYPE_DATE)
	private Date fetchedToDate;
	
	
	
	public Tag(){
	}	
	public Tag(HashtagEntity htag) {
		this.value = htag.getText();
	}


	
	@Override
	public String toString() {
		return "Tag [id=" + id + ", value=" + value + ", fetched=" 
				+ fetched + ", fetchedToDate=" + fetchedToDate + "]";
	}
	
	// getters and setters
	public boolean isFetched() {
		if (fetched == null)
			fetched = false;
		return fetched;
	}
	public void setFetched(boolean fetched) {
		this.fetched = fetched;
	}
	public Date getFetchedToDate() {
		return fetchedToDate;
	}
	public void setFetchedToDate(Date fetchedToDate) {
		this.fetchedToDate = fetchedToDate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
