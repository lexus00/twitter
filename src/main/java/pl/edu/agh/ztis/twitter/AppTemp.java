package pl.edu.agh.ztis.twitter;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import pl.edu.agh.ztis.twitter.controller.twitter.ConfigurationManager;
import pl.edu.agh.ztis.twitter.controller.twitter.LoggingStatusListener;
import pl.edu.agh.ztis.twitter.controller.twitter.TwitterLoaderManager;
import pl.edu.agh.ztis.twitter.controller.twitter.TwitterService;
import pl.edu.agh.ztis.twitter.util.KeyWords;
import twitter4j.IDs;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.StatusListener;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.User;
import twitter4j.conf.Configuration;


@SuppressWarnings("unused")
public class AppTemp {
	private static final Logger logger = Logger.getLogger(AppTemp.class);
	
	private static Twitter twitter;
	private static TwitterStream twitterStream;
	private static TwitterService twitterService;
	
	static {
		Configuration conf = ConfigurationManager.getInstance()
    			.getTwitterConfiguration();
		
    	twitter = new TwitterFactory(conf).getInstance();
    	twitterStream = new TwitterStreamFactory(conf).getInstance();
    	twitterService = TwitterService.getInstance();
	}
	
	// 'google' -> 20536157
	public static void main( String[] args ) throws Exception {
		String[] keywords = KeyWords.loadKeywords();
		
		new TwitterLoaderManager(keywords).start();
	
//		HibernateUtil.getSessionFactory().close();
	}

    
	@SuppressWarnings("deprecation")
	public static void testCheckResourceName() throws Exception{
		TwitterService ts = TwitterService.getInstance();
		Map<String, RateLimitStatus> before = ts.getRateLimitStatus();
		ts.lookupUsers("goog", "google");
		Map<String, RateLimitStatus> after = ts.getRateLimitStatus();
		
		
		for (Entry<String, RateLimitStatus> entryBefore : before.entrySet()){
			RateLimitStatus limitBefore = entryBefore.getValue();
			RateLimitStatus limitAfter = after.get(entryBefore.getKey());
			
			if (limitBefore.getRemaining() != limitAfter.getRemaining()){
				System.out.println(entryBefore.getKey() 
						+ " " + limitBefore.getRemaining()
						+ ", " + limitAfter.getRemaining());
			}
		}
	}
	
	public static void testGetTweetsByKeywords() throws TwitterException {
		Query query = new Query("MakeAmericaGreatAgain");
        query.setCount(100);
        QueryResult result;
        result = twitter.search(query);
        System.out.println("Getting Tweets...");
        List<Status> tweets = result.getTweets();
        for (Status status : tweets)
        	System.out.println(status.getId() + " " + status.getUser() 
        		+ " " + status.getText());
	}
	
	public void testUpdateStatus() throws TwitterException {
        Status status = twitter.updateStatus("hello world");
        System.out.println("Done");
	}
	
	public void testGetUserTweets() throws TwitterException {
		// First param of Paging() is the page number, second is the 
		// number per page (this is capped around 200 I think.
		Paging paging = new Paging(1, 100);
		List<Status> tweets = twitter.getUserTimeline("google",paging);
		for (Status status : tweets)
        	System.out.println(status.getId() + " " + status.getUser() 
        		+ " " + status.getText());
		
//		int pageno = 1;
//		String user = "cnn";
//		List statuses = new ArrayList();
//
//		while (true) {
//			int size = statuses.size();
//			Paging page = new Paging(pageno++, 100);
//			statuses.addAll(twitter.getUserTimeline(user, page));
//			if (statuses.size() == size)
//				break;
//		}
//		System.out.println("Total: " + statuses.size());
	}
	
	public void testGetUserProfile() throws TwitterException {		
		User user = twitter.showUser("google");
		if (user.getStatus() != null) {
		    System.out.println("@" + user.getScreenName() + 
		    		" - " + user.getDescription());
		} else {
		    // protected account
		    System.out.println("@" + user.getScreenName());
		}
    }
	
	public void testGetFollowersId() throws TwitterException {		
		IDs ids = twitter.getFollowersIDs("google", -1); // max 5000 IDs
		System.out.println(ids.getIDs().length + " ids.getNextCursor()=" + ids.getNextCursor());
		
//		long cursor =-1L;
//        IDs ids;
//        do {
//            ids = twitter.getFollowersIDs(cursor);
//            for(long userID : ids.getIDs()){
//                friendList.add(userID);
//            }
//        } while((cursor = ids.getNextCursor())!=0 );
	}
	
	public void testStreamApi() throws TwitterException {		
	    StatusListener listener = new LoggingStatusListener();
	    twitterStream.addListener(listener);	    
	    
//	    FilterQuery filter = new FilterQuery();
//	    filter.locations(bb);
//	    filter.language("en");	    
//	    twitterStream.filter();
	    
	    twitterStream.sample();
	}
	
	public void testExceededResorce() throws TwitterException{
		try {
			for (int i=0; i<5; i++){
				twitter.getRetweetsOfMe();
				Map<String, RateLimitStatus> limits = twitter.getRateLimitStatus();
//				System.out.println(limits);
				System.out.println(limits.get("/statuses/retweets_of_me"));	
			}
		} catch (TwitterException e) {
			System.out.println("have to sleep: " 
					+ e.getRateLimitStatus().getSecondsUntilReset()
					+ ", remaining=" + e.getRateLimitStatus().getRemaining());
			Map<String, RateLimitStatus> limits = twitter.getRateLimitStatus();
			System.out.println(limits.get("/statuses/retweets_of_me"));	
			System.exit(0);
		}
	}
}
