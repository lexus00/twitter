package pl.edu.agh.ztis.twitter.controller.twitter;

import java.util.*;

import com.google.common.collect.Sets;
import org.apache.log4j.Logger;

import pl.edu.agh.ztis.twitter.model.dao.HibernateUtil;
import pl.edu.agh.ztis.twitter.model.pojo.Tag;
import pl.edu.agh.ztis.twitter.model.pojo.UserInfo;
import twitter4j.IDs;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.User;

public class TwitterLoaderThread {
	private static final Logger logger = Logger.getLogger(TwitterLoaderThread.class);
	
	private TwitterService twitterService;
	private TwitterDAO dao;
	
	
	
	
	public TwitterLoaderThread(/*TwitterService ts*/){
		this.twitterService = TwitterService.getInstance();
		this.dao = new TwitterDAO();	
	}
	
	

	public boolean loadTag(String keyword){
		logger.info("load tag: " + keyword );
		Tag tag = dao.getTag(keyword);
		Date tagIsFetchedTo = tag == null ? null : tag.getFetchedToDate();
		
		try {
			boolean contunue_loop = true; 
			Query query = new Query(keyword);
			query.setCount(100);
			while ( query != null & contunue_loop){
				QueryResult queryResult = twitterService.getTweetsByKeywords(query);
				for (Status tweet : queryResult.getTweets()){
					dao.save(tweet);
					if (tagIsFetchedTo != null && tagIsFetchedTo.after(tweet.getCreatedAt()))
						contunue_loop = false;
				}
				query = queryResult.nextQuery();
			}
			tagSuccessfullyFetch(keyword);			
			return true;
		} catch (TwitterException | InterruptedException e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}
	
	/**
	 * @param userId
	 * @return is succeed
	 * @throws InterruptedException 
	 */
	public boolean loadUserAllData(long userId) {
		logger.info("load User All Data= " + userId );
		UserInfo user = HibernateUtil.getById(UserInfo.class, userId);
		
		if ( user == null){
			loadUserProfile(userId);
		} else if (user.isFetched() && user.getFetchedToDate() != null) {
			return true;
		}
		
		List<Thread> threads = Arrays.asList(new Thread[]{	
			new Thread (() -> {loadUserTweets(userId);}),
			new Thread (() -> {loadUserFollowers(userId);}),
			new Thread (() -> {loadUserFriends(userId);}),
		});

		threads.stream().forEach(t -> t.start() );
		
		try {
			for (Thread t : threads)
				t.join();			
			userSuccessfullyFetch(userId);			
			return true;
			
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}
	
	public boolean loadUserProfile(long userId){
		try {
			User user = twitterService.getUserProfile(userId);
			dao.save(user);
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}
	
	public boolean loadUserTweets(long userId){
		try {
			int pageno = 1;
			while(true){
				List<Status> list = twitterService.getUserTweets(userId, pageno, 100);
				for (Status tweet : list)
					dao.save(tweet);
				pageno++;
				if (list.isEmpty())
					break;
			}			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}
	
	public boolean loadUserFollowers(long userId){
		try {
			long cursor = -1;
			while (cursor != 0){
				IDs ids = twitterService.getUserFollowers(userId, cursor);
				dao.saveFollowerRelation(userId, ids);
				cursor = ids.getNextCursor();
			}			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}
	
	public boolean loadUserFriends(long userId){
		try {
			long cursor = -1;
			while (cursor != 0){
				IDs ids = twitterService.getUserFriends(userId, cursor);
				dao.saveFriendRelation(userId, ids);
				cursor = ids.getNextCursor();
			}			
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return false;
		}
	}
	
	public void userSuccessfullyFetch(long userId){
		UserInfo user = HibernateUtil.getById(UserInfo.class, userId);
		user.setFetched(true);
		user.setFetchedToDate(new Date());
		HibernateUtil.saveOrUpdate(user);
	}
	public void tagSuccessfullyFetch(String keyword){
		Tag tag = dao.getTag(keyword);
		if (tag == null){
			tag = new Tag();
			tag.setValue(keyword);
		}
		logger.info("mark succ " + keyword + " " + tag);
		tag.setFetched(true);
		tag.setFetchedToDate(new Date());
		HibernateUtil.saveOrUpdate(tag);
	}



	public Set<Long> lookupUsersId(List<String> keywords) {
		Set<Long> userIds = new HashSet<>();
		List<String> query = new ArrayList<>();
		
		for (String keyword : Sets.newHashSet(keywords)){
			query.add(keyword);
			if (query.size() > 98){
				lookUpForUserId(userIds, query);
			}
		}
		if (!query.isEmpty()) {
			lookUpForUserId(userIds, query);
		}
		
		return userIds;
	}

	private void lookUpForUserId(Set<Long> userIds, List<String> query) {
		try {
            ResponseList<User> result = twitterService
                    .lookupUsers(query.toArray(new String[query.size()]));
            for (User user : result){
                dao.save(user);
                userIds.add(user.getId());
            }
            query.clear();
        } catch (TwitterException | InterruptedException e) {
            logger.error(e.getMessage(), e);
        }
	}
}
